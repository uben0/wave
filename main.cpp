#include <cmath>
#include <fstream>
#include "klibrary/KLibrary.hpp"
#include "json.hpp"

double distance_between(
	double x1,
	double y1,
	double x2,
	double y2
) {
	return std::sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}


KColor strength_to_color(unsigned int strength) {
	if (strength < 256) {
		return KColor(0, 0, strength);
	}
	else if (strength < 512) {
		return KColor(0, strength - 256, 255);
	}
	else if (strength < 768) {
		return KColor(0, 255, 768 - strength);
	}
	else if (strength < 1024) {
		return KColor(strength - 768, 255, 0);
	}
	else if (strength < 1280) {
		return KColor(255, 1280 - strength, 0);
	}
	else if (strength < 1536) {
		return KColor(255, 0, strength - 1280);
	}
	else if (strength < 1792) {
		return KColor(255, strength - 1536, 255);
	}
	else {
		return KColor::White;
		if (strength % 2 == 0) {
			return KColor(255, 255, 255);
		}
		else {
			return KColor(0, 0, 0);
		}
	}
}

int main(int, const char*[]) {
	constexpr unsigned int width = 1280;
	constexpr unsigned int height = 720;
	// constexpr double PI = 3.14159265;
	double signalMap[height][width] = {};
	KSurface surface(width, height);

	struct Source {
		unsigned int x, y;
		unsigned int amplitude;
		unsigned int waveLength;
	};

	std::vector<Source> sources;

	if (true) {
		std::ifstream file("sources.json");
		Json json(file);
		if (not json.match(Json::Array({
			Json::Object({
				{"x", Json::Number()},
				{"y", Json::Number()},
				{"amplitude", Json::Number()},
				{"wave length", Json::Number()}
			})
		}))) {
			std::cerr << "ill-formed file data" << std::endl;
			return EXIT_FAILURE;
		}

		for (
			auto it = json.array().begin();
			it != json.array().end();
			it++
		) {
			sources.push_back({
				(unsigned int)(*it)["x"].number(),
				(unsigned int)(*it)["y"].number(),
				(unsigned int)(*it)["amplitude"].number(),
				(unsigned int)(*it)["wave length"].number()
			});
		}
	}

	KWindow window("wave", width, height, {}, KWindow::Hidden);

	for (unsigned int x = 0; x < width; x++) {
		window.event.update();
		if (window.event.quit) return EXIT_FAILURE;

		std::cout << '\r';
		std::cout.width(3);
		std::cout << (x * 100 / width) << "% " << std::flush;

		for (unsigned int y = 0; y < height; y++) {
			double phaseX = 0;
			double phaseY = 0;
			for (auto it = sources.begin(); it != sources.end(); it++) {
				register double dist = distance_between(it->x, it->y, x, y);
				if (dist != 0) {
					phaseX += std::cos(dist / it->waveLength) * it->amplitude / dist;
					phaseY += std::sin(dist / it->waveLength) * it->amplitude / dist;
				}
			}
			signalMap[y][x] = distance_between(phaseX, phaseY, 0, 0);
		}
	}
	std::cout << "\r100%" << std::endl;

	for (unsigned int x = 0; x < width; x++) {
		for (unsigned int y = 0; y < height; y++) {
			surface.set_pixel(x, y, strength_to_color(signalMap[y][x]));
		}
	}
	surface.blur(2);

	for (auto it = sources.begin(); it != sources.end(); it++) {
		surface.fill({it->x - 8, it->y - 1, 16, 2}, KColor::Black);
		surface.fill({it->x - 1, it->y - 8, 2, 16}, KColor::Black);
	}

	KSurface graph(width, 400, KColor(40, 40, 40));
	for (unsigned int i = 0; i < width; i++) {
		double signal = std::pow(signalMap[0][i], 2) / 200;
		graph.fill({i, graph.h - signal, 1, signal}, KColor(0, 150, 255));
		if (signal < 400) {
			graph.set_pixel(i, 400 - signal, KColor::White);
		}
	}

	KWindow graphWindow("graph", graph.w, graph.h);
	KTimer timer(KTimer::Fps30);
	window.show();
	while (window.is_opened() or graphWindow.is_opened()) {

		graphWindow.print(graph);
		graphWindow.display();
		window.print(surface);
		window.display();
		timer.wait();

		window.event.update();
		if (
			window.event.quit or
			window.event.close or
			window.event[KKey::Q]
		) {
			window.close();
		}
		graphWindow.event.update();
		if (
			graphWindow.event.quit or
			graphWindow.event.close or
			graphWindow.event[KKey::Q]
		) {
			graphWindow.close();
		}
	}

	return EXIT_SUCCESS;
}
