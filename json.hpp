#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <map>


class Json {
public:

	enum Type {
		TYPE_NULL,
		TYPE_BOOLEAN,
		TYPE_NUMBER,
		TYPE_STRING,
		TYPE_ARRAY,
		TYPE_OBJECT
	};

	typedef Json                     Null;
	typedef bool                  Boolean;
	typedef long int               Number;
	typedef std::string            String;
	typedef std::vector<Json>       Array;
	typedef std::map<String, Json> Object;

	class bad_syntax: std::exception {};
	class bad_type  : std::exception {};

private:
	enum Type m_type;
	union {
		Boolean boolean;
		Number   number;
		String * string;
		Array  *  array;
		Object * object;
	} m_value;

public:

	Json(                   );
	Json(      Boolean value);
	Json(      Number  value);
	Json(const String& value);
	Json(const Array & value);
	Json(const Object& value);

	Json(std::istream& istream);
	Json(const Json& src);
	Json& operator=(const Json& src);
	void clear();
	~Json();

	/* test type of json value */
	enum Type  type() const;
	bool is_null   () const;
	bool is_boolean() const;
	bool is_number () const;
	bool is_string () const;
	bool is_array  () const;
	bool is_object () const;

	/* access json value as c++ variables */
	Boolean & boolean();
	Number  &  number();
	String  &  string();
	Array   &   array();
	Object  &  object();
	/* access json value as const c++ variables */
	const Boolean & boolean() const;
	const Number  &  number() const;
	const String  &  string() const;
	const Array   &   array() const;
	const Object  &  object() const;


	/* parses json code from istream into the json class */
	void parse(std::istream& istream);
	/* prints json code to ostream from the json class */
	void print(
		std::ostream& ostream,
		bool pretty = false,
		unsigned int indent = 0
	) const;



	/* individual parsers */
	/* directly converts json code to c++ variable */
	static void parse_null   (std::istream& istream                  );
	static void parse_boolean(std::istream& istream, Boolean& boolean);
	static void parse_number (std::istream& istream, Number&   number);
	static void parse_string (std::istream& istream, String&   string);
	static void parse_array  (std::istream& istream, Array&     array);
	static void parse_object (std::istream& istream, Object&   object);

	/* individual printers */
	/* directly converts c++ variable to json code */
	static void print_null(
		std::ostream& ostream
	);
	static void print_boolean(
		std::ostream& ostream,
		const Boolean& boolean
	);
	static void print_number(
		std::ostream& ostream,
		const Number& number
	);
	static void print_string(
		std::ostream& ostream,
		const String& string
	);
	static void print_array(
		std::ostream& ostream,
		const Array& array,
		bool pretty = false,
		unsigned int indent = 0
	);
	static void print_object(
		std::ostream& ostream,
		const Object& object,
		bool pretty = false,
		unsigned int indent = 0
	);
	
	bool match(const Json& model) const;

	Json& operator[](unsigned int index);
	const Json& operator[](unsigned int index) const;
	Json& operator[](const String& key);
	const Json& operator[](const String& key) const;

private:
	static void discard_space(std::istream& istream        );
	static void seek         (std::istream& istream, char c);

	static void print_indent(
		std::ostream& ostream,
		unsigned int level,
		const char* chars = "    "
	);
};

std::istream& operator>>(std::istream& istream,       Json& dst);
std::ostream& operator<<(std::ostream& ostream, const Json& src);

