# Wave - Interferences simulator

Requiers the SDL2 library to compile.
Compile using the `Makefile`.
Place emmission sources using the `sources.json` file.

![screenshot](https://gitlab.com/uben0/wave/raw/master/screenshot.bmp)
