#include <memory>
#include "json.hpp"


Json::Json(const Object& value) {
	m_type = TYPE_OBJECT;
	m_value.object = new Object(value);
}

Json::Json(const Array& value) {
	m_type = TYPE_ARRAY;
	m_value.array = new Array(value);
}

Json::Json(const String& value) {
	m_type = TYPE_STRING;
	m_value.string = new String(value);
}

Json::Json(Number value) {
	m_type = TYPE_NUMBER;
	m_value.number = value;
}

Json::Json(Boolean value) {
	m_type = TYPE_BOOLEAN;
	m_value.boolean = value;
}

Json::Json() {
	m_type = TYPE_NULL;
}

Json::Json(const Json& src) {
	m_type = TYPE_NULL;
	operator=(src);
}

Json::Json(std::istream& istream): Json() {
	parse(istream);
}

Json::~Json() {
	clear();
}

void Json::clear() {
	switch (m_type) {
		case TYPE_OBJECT:
		delete m_value.object;
		break;
		case TYPE_ARRAY:
		delete m_value.array;
		break;
		case TYPE_STRING:
		delete m_value.string;
		break;
		default: break;
	}
	m_type = TYPE_NULL;
}

enum Json::Type Json::type() const {
	return m_type;
}

bool Json::is_object() const {
	return m_type == TYPE_OBJECT;
}

bool Json::is_array() const {
	return m_type == TYPE_ARRAY;
}

bool Json::is_string() const {
	return m_type == TYPE_STRING;
}

bool Json::is_number() const {
	return m_type == TYPE_NUMBER;
}

bool Json::is_boolean() const {
	return m_type == TYPE_BOOLEAN;
}


const Json::Object& Json::object() const {
	if (m_type != TYPE_OBJECT) throw bad_type();
	return *m_value.object;
}
Json::Object& Json::object() {
	if (m_type != TYPE_OBJECT) throw bad_type();
	return *m_value.object;
}

const Json::Array& Json::array() const {
	if (m_type != TYPE_ARRAY) throw bad_type();
	return *m_value.array;
}
Json::Array& Json::array() {
	if (m_type != TYPE_ARRAY) throw bad_type();
	return *m_value.array;
}

const Json::String& Json::string() const {
	if (m_type != TYPE_STRING) throw bad_type();
	return *m_value.string;
}

Json::String& Json::string() {
	if (m_type != TYPE_STRING) throw bad_type();
	return *m_value.string;
}

const Json::Number& Json::number() const {
	if (m_type != TYPE_NUMBER) throw bad_type();
	return m_value.number;
}
Json::Number& Json::number() {
	if (m_type != TYPE_NUMBER) throw bad_type();
	return m_value.number;
}

const Json::Boolean& Json::boolean() const {
	if (m_type != TYPE_BOOLEAN) throw bad_type();
	return m_value.boolean;
}
Json::Boolean& Json::boolean() {
	if (m_type != TYPE_BOOLEAN) throw bad_type();
	return m_value.boolean;
}

void Json::parse_object(std::istream& istream, Object& object) {
	seek(istream, '{');
	discard_space(istream);

	if (istream.peek() == '}') {
		istream.ignore();
		return;
	}

	String key;
	while (true) {
		parse_string(istream, key);

		seek(istream, ':');

		object.insert(std::pair<String, Json>(key, Json(istream)));

		discard_space(istream);
		if (istream.peek() == '}') {
			istream.ignore();
			return;
		}
		seek(istream, ',');
	}
}

void Json::parse_array(std::istream& istream, Array& array) {
	seek(istream, '[');
	discard_space(istream);

	if (istream.peek() == ']') {
		istream.ignore();
		return;
	}

	while (true) {
		array.push_back(Json(istream));

		discard_space(istream);
		if (istream.peek() == ']') {
			istream.ignore();
			return;
		}
		if (istream.get() != ',') throw bad_syntax();
	}
}

void Json::parse_string(std::istream& istream, String& string) {
	string.clear();
	seek(istream, '\"');
	char buffer;
	while ((buffer = istream.get()) != '\"' and not istream.eof()) {
		if (buffer == '\\') {
			buffer = istream.get();
			if (istream.eof()) throw bad_syntax();
			switch (buffer) {
				case 'b': string += '\b'; break;
				case 'f': string += '\f'; break;
				case 'n': string += '\n'; break;
				case 'r': string += '\r'; break;
				case 't': string += '\t'; break;
				default: string += buffer;
			}
		}
		else {
			string += buffer;
		}
	}
	if (buffer != '\"') throw bad_syntax();
}

void Json::parse_number(std::istream& istream, Number& number) {
	discard_space(istream);
	bool negative = false;
	if (istream.peek() == '-') {
		istream.ignore();
		negative = true;
	}
	if (not std::isdigit(istream.peek())) throw bad_syntax();
	number = 0;
	if (istream.peek() == '0') {
		istream.ignore();
		return;
	}

	while (std::isdigit(istream.peek()) and not istream.eof()) {
		number *= 10;
		number += istream.get() - '0';
	}
	if (negative) number *= -1;
}

void Json::parse_boolean(std::istream& istream, Boolean& boolean) {
	discard_space(istream);
	if (istream.peek() == 't') {
		if (
			istream.get() != 't' or
			istream.get() != 'r' or
			istream.get() != 'u' or
			istream.get() != 'e'
		) {
			throw bad_syntax();
		}
		boolean = true;
	}
	else if (istream.peek() == 'f') {
		if (
			istream.get() != 'f' or
			istream.get() != 'a' or
			istream.get() != 'l' or
			istream.get() != 's' or
			istream.get() != 'e'
		) {
			throw bad_syntax();
		}
		boolean = false;
	}
	else throw bad_syntax();
}

void Json::parse_null(std::istream& istream) {
	seek(istream, 'n');
	if (
		istream.get() != 'u' or
		istream.get() != 'l' or
		istream.get() != 'l'
	) {
		throw bad_syntax();
	}
}

void Json::parse(std::istream& istream) {
	discard_space(istream);
	switch (istream.peek()) {

		// OBJECT
		case '{': {
			std::unique_ptr<Object> tmp(new Object());
			parse_object(istream, *tmp);
			m_value.object = tmp.release();
			m_type = TYPE_OBJECT;
		} break;

		// ARRAY
		case '[': {
			std::unique_ptr<Array> tmp(new Array());
			parse_array(istream, *tmp);
			m_value.array = tmp.release();
			m_type = TYPE_ARRAY;
		} break;

		// STRING
		case '"': {
			std::unique_ptr<String> tmp(new String());
			parse_string(istream, *tmp);
			m_value.string = tmp.release();
			m_type = TYPE_STRING;
		} break;

		// NUMBER
		case '-':
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9': {
			parse_number(istream, m_value.number);
			m_type = TYPE_NUMBER;
		} break;

		// BOOLEAN
		case 't':
		case 'f': {
			parse_boolean(istream, m_value.boolean);
			m_type = TYPE_BOOLEAN;
		} break;

		// NULL
		case 'n': {
			parse_null(istream);
			m_type = TYPE_NULL;
		} break;

		default: throw bad_syntax();
	}
}

void Json::print_object(
	std::ostream& ostream,
	const Object& object,
	bool pretty,
	unsigned int indent
) {
	bool comma = false;
	ostream << '{';
	indent += 1;
	for (auto elem = object.begin(); elem != object.end(); ++elem) {
		if (comma) ostream << ',';
		if (pretty) ostream << '\n';
		comma = true;
		print_indent(ostream, indent);
		print_string(ostream, elem->first);
		ostream << ':';
		if (pretty) ostream << ' ';
		elem->second.print(ostream, pretty, indent);
	}
	indent -= 1;
	if (pretty) {
		ostream << '\n';
		print_indent(ostream, indent);
	}
	ostream << '}';
}

void Json::print_array(
	std::ostream& ostream,
	const Array& array,
	bool pretty,
	unsigned int indent
) {
	bool comma = false;
	ostream << '[';
	indent += 1;
	for (auto elem = array.begin(); elem != array.end(); elem++) {
		if (comma) ostream << ',';
		comma = true;
		if (pretty) {
			ostream << '\n';
			print_indent(ostream, indent);
		}
		elem->print(ostream, pretty, indent);
	}
	indent -= 1;
	if (pretty) {
		ostream << '\n';
		print_indent(ostream, indent);
	}
	ostream << ']';
}

void Json::print_string(std::ostream& ostream, const String& string) {
	ostream << '\"';
	auto end = string.end();
	for (auto c = string.begin(); c != end; c += 1) {
		switch (*c) {
			case '\"': ostream << "\\\""; break;
			case '\\': ostream << "\\\\"; break;
			case '\b': ostream << "\\b"; break;
			case '\f': ostream << "\\f"; break;
			case '\n': ostream << "\\n"; break;
			case '\r': ostream << "\\r"; break;
			case '\t': ostream << "\\t"; break;
			default: ostream << *c;
		}
	}
	ostream << '\"';
}

void Json::print_number(std::ostream& ostream, const Number& number) {
	ostream << number;
}

void Json::print_boolean(std::ostream& ostream, const Boolean& boolean) {
	ostream << (boolean ? "true" : "false");
}

void Json::print_null(std::ostream& ostream) {
	ostream << "null";
}

void Json::print(
	std::ostream& ostream,
	bool pretty,
	unsigned int indent
) const {
	switch (m_type) {
		case TYPE_OBJECT:
		print_object(ostream, *m_value.object, pretty, indent);
		break;
		case TYPE_ARRAY:
		print_array(ostream, *m_value.array , pretty, indent);
		break;
		case TYPE_STRING:
		print_string(ostream, *m_value.string);
		break;
		case TYPE_NUMBER:
		print_number(ostream, m_value.number);
		break;
		case TYPE_BOOLEAN:
		print_boolean(ostream, m_value.boolean);
		break;
		case TYPE_NULL:
		print_null(ostream);
		break;
		default:
		throw bad_type();
	}
}


std::istream& operator>>(std::istream& istream, Json& dst) {
	dst.parse(istream);
	return istream;
}

std::ostream& operator<<(std::ostream& ostream, const Json& src) {
	src.print(ostream, true);
	return ostream;
}

Json& Json::operator=(const Json& src) {
	clear();
	m_type = src.m_type;
	switch (m_type) {
	
		case TYPE_OBJECT:
		m_value.object = new Object(*src.m_value.object);
		break;
	
		case TYPE_ARRAY:
		m_value.array = new Array(*src.m_value.array);
		break;
		
		case TYPE_STRING:
		m_value.string = new String(*src.m_value.string);
		break;

		default: m_value = src.m_value;
	}
	return *this;
}

void Json::discard_space(std::istream& istream) {
	while (std::isspace(istream.peek()) and not istream.eof()) istream.ignore();
}

void Json::seek(std::istream& istream, char c) {
	discard_space(istream);
	if (istream.peek() != c) throw bad_syntax();
	istream.ignore();
}

void Json::print_indent(
	std::ostream& ostream,
	unsigned int level,
	const char* chars
) {
	for (unsigned int i = 0; i < level; i++) ostream << chars;
}

bool Json::match(const Json& model) const {
	switch (model.m_type) {
		case TYPE_OBJECT: {
		
			if (not is_object()) {
				return false;
			}
			if (model.object().size() == 0) {
				return true;
			}
			
			for (
				Object::const_iterator iterator1 = model.object().begin();
				iterator1 != model.object().end();
				iterator1++
			) {
			
				if (iterator1->first == "") {
					for (
						Object::const_iterator iterator2 = object().begin();
						iterator2 != object().end();
						iterator2++
					) {
						if (not iterator2->second.match(iterator1->second)) {
							return false;
						}
					}
				}
				else {
					Object::const_iterator iterator2;
					for (
						iterator2 = object().begin();
						iterator2 != object().end();
						iterator2++
					) {
						if (iterator1->first == iterator2->first) break;
					}
					if (
						iterator2 == object().end() or
						not iterator2->second.match(iterator1->second)
					) return false;
				}
			}
			return true;
		}
		break;
		case TYPE_ARRAY: {
			
			if (not is_array()) {
				return false;
			}
			if (model.array().size() == 0) {
				return true;
			}
			
			for (
				Array::const_iterator iterator1 = array().begin();
				iterator1 != array().end();
				iterator1++
			) {
				Array::const_iterator iterator2;
				for (
					iterator2 = model.array().begin();
					iterator2 != model.array().end();
					iterator2++
				) {
					if (iterator1->match(*iterator2)) break;
				}
				if (iterator2 == model.array().end()) {
					return false;
				}
			}
			return true;
		}
		break;
		case TYPE_NULL: return true;
		default: return model.type() == type();
	}
	return false;
}

Json& Json::operator[](unsigned int index) {
	return array()[index];
}

const Json& Json::operator[](unsigned int index) const {
	return array().at(index);
}

Json& Json::operator[](const String& key) {
	return object()[key];
}

const Json& Json::operator[](const String& key) const {
	return object().at(key);
}


